FROM python:3.6-alpine
LABEL Maintainer "paterne paterneatango@gmail.com" 
WORKDIR /opt
RUN pip install flask==1.1.2
COPY . .
EXPOSE 8080
ENV ODOO_URL="https://odod.com"
ENV PGADMIN_URL="https://pdgadmin.com"
ENTRYPOINT ["python","app.py"]